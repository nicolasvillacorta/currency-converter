package com.example.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void convertir(View view){

        EditText dolares = (EditText) findViewById(R.id.pesosEntrada);

        double dolaresAConvertir = Double.parseDouble(dolares.getText().toString());
        double pesos = dolaresAConvertir*59.83;

        Toast.makeText(this,"U$D" + dolares.getText() + " equivale a " + pesos + "$." ,Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
